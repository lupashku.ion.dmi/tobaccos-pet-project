 
 function Brand(id, name, country, rank) {
     this.id = id;
     this.name = name;
     this.country = country;
     this.rank = rank;
 };

function Tobacco(id, weight, taste, strength, price, expirationDate, brand, active) {
    this.id = id;
    this.weight = weight;
    this.taste = taste;
    this.strength = strength;
    this.price = price;
    this.expirationDate = expirationDate;
    this.brand = brand;
    this.active = active;
};

const STRENGTH = {
    'LIGHT': 'LIGHT',
    'MEDIUM': 'MEDIUM',
    'STRONG': 'STRONG',
};


 const brand1 = new Brand(1, 'DarkSide', 'Russia', 8);
 const brand2 = new Brand(2, 'DailyHookah', 'Russia', 6);
 const brand3 = new Brand(3, 'Nashi', 'Ukraine', 10);

 const brands = [brand1, brand2, brand3];


 let tobaccos = [];




document.addEventListener('DOMContentLoaded', function() {
    let selectTag = document.getElementById("brand")
    brands.map( (obj) => {
        let opt = document.createElement("option");
        opt.value = obj.id; // the index
        opt.innerHTML = obj.name;
        selectTag.append(opt);
    });

    let selectStrength = document.getElementById("strength")
    Object.keys(STRENGTH).forEach(
        function(currentKey) {
            let opt = document.createElement("option");
            opt.value = currentKey; // the index
            opt.innerHTML = STRENGTH[currentKey];
            selectStrength.append(opt);
        }
    );

    let tableTag = document.getElementById("table")
    tobaccos.map( (obj) => {
        addRowToTable(obj, tableTag);
    });
}, false);


function givenData() {
    let name = document.getElementById('name').value;
    let weight = document.getElementById('weight').value;
    let strength = document.getElementById('strength').value;
    let brandId = document.getElementById('brand').value;
    let price = document.getElementById('price').value;
    let date = document.getElementById('date').value;
    let available = document.getElementById('available').checked;

    //validate data
    //  12
    parsedWeight = parseInt(weight);
    parsedPrice = parseInt(price);
    parsedDate = new Date(date);
    const newDate = new Date();
    const numbersInSringExist = /[0-9]+/gm.test(name);
    const nameErrors = [];
    const weightErrors = [];
    const priceErrors = [];
    const dateErrors = [];
    const strengthErrors = [];
    const brandErrors = [];
    if (name.length == 0) {
        nameErrors.push("Your Name field is empty!")}
    if (numbersInSringExist === true) {
        nameErrors.push("Name containes numbers!")} 
    if (name.length <= 3) {
        nameErrors.push("The min size of name is 3. Your name length is " + name.length)} 
    if (name.length >=50) {
        nameErrors.push(" The max size of name is 50. Your name length is " + name.length)} 
    if (parsedWeight < 0.1) {
        weightErrors.push(" The min value of weight is 0.1. Your weight is " + parsedWeight);}
    if (isNaN(parsedWeight)) {
        weightErrors.push(" The min value of weight is 0.1.");}
    if (parsedPrice < 0.1) {
        priceErrors.push(" The min value of price is 0.1. Your weight is " + parsedPrice);} 
    if (isNaN(parsedPrice)) {
        priceErrors.push(" The min value of price is 0.1")}
    if (strength.length == 0) {
        strengthErrors.push("Select  tobacco strength please!")}
    if (brandId.length == 0) {
        brandErrors.push("Select tobacco brand please!")}
    if (parsedDate <= newDate) {
        dateErrors.push("You cannot enter this product. You can enter it if your expiratiion date is later then today date. Your product expiration date is " + parsedDate);}
    removeAll();
    if (
        nameErrors.length > 0
        || weightErrors.length > 0
        || priceErrors.length > 0
        || dateErrors.length > 0
        || brandErrors.length > 0
        || strengthErrors.length > 0
    ) {
        addelement(nameErrors);
        addelement(weightErrors);
        addelement(priceErrors);
        addelement(dateErrors);
        addelement(brandErrors);
        addelement(strengthErrors);
        return
    }

   
 
    // find max id from tobaccos array
    const ids = tobaccos.map(object => {
        return object.id;
    });
    const max = ids.length > 0 ? Math.max(...ids) : 0;
    
    let newTobacco =  new Tobacco(max + 1, weight, name, strength, price, date, findBrandById(brandId), available);
    // tobaccos.push(newTobacco);
    // let tableTag = document.getElementById("table")
    // addRowToTable(newTobacco, tableTag);
    saveNewTobacco(newTobacco);
    let tableTag = document.getElementById("table");
    fetch('http://localhost:3000/tobaccos')
  .then(function(response) {
    return response.json();
  })
  .then(function(myJson) {
    myJson.forEach(tobaccos => {
        if (!ids.some(e => e === tobaccos.id)) {
            addRowToTable(tobaccos, tableTag);
        }
    });
    tobaccos = myJson;
  });

}

function saveNewTobacco(tobacco) {
    var jsonObj = JSON.stringify(tobacco);
    fetch('http://localhost:3000/tobaccos', {
	method: 'POST',
	body: jsonObj, // The data
	headers: {
		'Content-type': 'application/json' // The type of data you're sending
	}
});
}

 function addelement(errors) {
    let completelist= document.getElementById("thelist");
    if (errors.length > 0) {
         completelist.innerHTML += "<li> " + errors.join("") + "</li>";
    }
}
function removeAll(){
    let emptyList = document.getElementById("thelist");
    emptyList.innerHTML = "";
}

function findBrandById (id) {
    // id = "3"
    // obj.id = 3
    return brands.find(obj => obj.id == id);
}

function addRowToTable(obj, tableTag) {
    let row = tableTag.insertRow();
    // Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);
    var cell5 = row.insertCell(4);
    var cell6 = row.insertCell(5);
    var cell7 = row.insertCell(6);
    var cell8 = row.insertCell(7);
    // Add some text to the new cells:
    cell1.innerHTML = obj.id;
    cell2.innerHTML = obj.taste;
    cell3.innerHTML = obj.weight;
    cell4.innerHTML = obj.strength;
    cell5.innerHTML = obj.price;
    cell6.innerHTML = obj.expirationDate;
    cell7.innerHTML = obj.brand.name;
    cell8.innerHTML = obj.active;
}




window.addEventListener("load", function(){
    let tableTag = document.getElementById("table")
    fetch('http://localhost:3000/tobaccos')
  .then(function(response) {
    return response.json();
  })
  .then(function(myJson) {
    myJson.forEach(tobaccos => addRowToTable(tobaccos, tableTag));
    tobaccos = myJson;
  });
});


